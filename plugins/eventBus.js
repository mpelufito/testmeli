const eventBus = {}

eventBus.install = function (Vue) {
  Vue.prototype.$bus = new Vue()
}

export default eventBus

// import EventBus from '@/plugins/eventBus'
// Vue.use(EventBus)
