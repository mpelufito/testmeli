import { Router } from 'express'
import searchModule from './modules/moduleSearch'
const router = Router()

router.get('/items', function (req, res, next) {
  searchModule.searchItems(req.query.search)
    .then(dataResponse => res.send(dataResponse))
})

/* GET user by ID. */
router.get('/items/:id', function (req, res, next) {
  /* searchModule.searchItem(req.params.id)
    .then(dataResponse => res.send(dataResponse)) */
  Promise.all([
    searchModule.searchItem(req.params.id),
    searchModule.itemDescription(req.params.id)
  ])
    .then(values => {
      values[0].description = values[1]
      // console.log('id de current ', values[0].price.currency)
      searchModule.searchCurrent(values[0])
        .then(dataResponse => {
          values[0].price.currency = dataResponse.currency
          values[0].price.decimals = dataResponse.decimals
          // console.log(dataResponse)
          res.send(values[0])
        })
    })
    .catch(reason => {
      console.log(reason)
    })
})

export default router
