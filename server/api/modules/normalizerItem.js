/* class NewItem {
  constructor (id, title, currency, price, condition, picture, shipping) {
    this.id = id
    this.title = title
    this.price = {
      'currency': currency,
      'amount': price
    }
    this.condition = condition
    this.picture = picture
    this.free_shipping = shipping
  }
} */
/* const normalizerItemList = (Item) => {
  const newListItem = []
  for (let item of Item.results) {
    newListItem.push(new NewItem(item.id, item.title, item.currency_id, item.price, item.condition, item.thumbnail, item.shipping.free_shipping))
  }
  return newListItem
} */

const normalizerItem = {
  items: (oldObj, dataDescription) => ({
    item: {
      id: oldObj.id,
      title: oldObj.title
    },
    price: {
      amount: oldObj.price,
      currency: oldObj.currency_id
    },
    picture: oldObj.pictures[0].url,
    condition: oldObj.condition,
    free_shipping: oldObj.shipping.free_shipping,
    sold_quantity: oldObj.sold_quantity,
    description: dataDescription,
    author: {
      'name': 'Matias',
      'lastname': 'Blayer'
    }
  })
}

export default normalizerItem
