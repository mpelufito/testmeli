
class NewItemList {
  constructor (id, title, currencyId, price, condition, picture, shipping) {
    this.id = id
    this.title = title
    this.price = {
      'currency': currencyId.simbol,
      'amount': price,
      'decimals': currencyId.decimals
    }
    this.condition = condition
    this.picture = picture
    this.free_shipping = shipping
  }
}

const normalizerItemList = (listItem) => {
  const newListItem = []
  for (let item of listItem.results) {
    let itemAsing = new NewItemList(item.id, item.title, item.currency_id, item.price, item.condition, item.thumbnail, item.shipping.free_shipping)
    newListItem.push(itemAsing)
  }
  // console.log(newListItem)
  return newListItem
}

const normalizerItems = {
  items: (oldObj) => ({
    items: normalizerItemList(oldObj),
    categories: oldObj.available_filters[0].values[0].name,
    author: {
      'name': 'Matias',
      'lastname': 'Blayer'
    }
  })
}

export default normalizerItems
