
import trae from 'trae'
import normalizerList from './normalizerList.js'
import normalizerItem from './normalizerItem.js'

const search = {
  baseUrl: 'https://api.mercadolibre.com'
}

search.searchItems = function (dataItem) {
  return trae.get(`${this.baseUrl}/sites/MLA/search`, { params: { q: dataItem, limit: 4 } })
    .then((response) => {
      const newData = normalizerList.items(response.data)
      return newData
    })
    .catch((err) => {
      console.error(err)
    })
}

search.searchItem = function (idItem) {
  return trae.get(`${this.baseUrl}/items`, { params: { id: idItem } })
    .then((response) => {
      const newDataItem = normalizerItem.items(response.data)
      // console.log(newDataItem)
      return newDataItem
    })
    .catch((err) => {
      console.error(err)
    })
}

search.searchCurrent = function (currentID) {
  return trae.get(`https://api.mercadolibre.com/currencies/${currentID.price.currency}`, {})
    .then((response) => {
      // console.log('currency data', response.data.symbol, 'decimals', response.data.decimal_places)
      return {'currency': response.data.symbol, 'decimals': response.data.decimal_places}
    })
    .catch((err) => {
      console.error(err)
    })
}

search.itemDescription = function (idDesciption) {
  return trae.get(`https://api.mercadolibre.com/items/${idDesciption}/description`, {})
    .then((response) => {
      return response.data.plain_text
    })
    .catch((err) => {
      console.error(err)
    })
}

export default search
