import trae from 'trae'

const searchCurrent = {
  searchCurrentData: function (dataCurrentId) {
    return trae.get(`https://api.mercadolibre.com/currencies/${dataCurrentId}`, {})
      .then((response) => {
        return {'currency': response.data.simbol, 'decimals': response.data.decimals_places}
      })
      .catch((err) => {
        console.error(err)
      })
  }
}

export default searchCurrent
